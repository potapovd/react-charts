import React from 'react'

export const getReqBody = (page, flag = '') => {
	let reqBody = {}
	if (page === 'jobs') {
		reqBody = {
			entity: 'Job',
			strictCheck: true,
			maxDBResults: 500
		}
	} else if (page === 'jobs-tracker-modal-jobTitle') {
		reqBody = {
			jobIdsFilter: [`${flag}`],
			stepFilter: ['CV Submitted', 'Round 1', 'Round 2'],
			rejected: false
		}
	} else if (page === 'jobs-tracker-modal-oneUser') {
		reqBody = {
			jobId: `${flag.activeJobId}`,
			candidateId: `${flag.activeOneUser}`
		}
	} else if (page === 'offers-tracker') {
		reqBody = {
			job: {
				employerCompanyId: flag
			}
		}
	} else if (page === 'analytics-time-throughput-jobmetrics') {
		reqBody = {
			entity: 'Job',
			andConditionMap: {},
			strictCheck: true,
			maxDBResults: 500
		}
	} else if (page === 'analytics-time-throughput-candid-dashb') {
		reqBody = {
			entity: 'Job',
			strictCheck: true,
			maxDBResults: 500
		}
	} else if (page === 'auth') {
		reqBody = {
			email: flag.email,
			password: flag.password
		}
	} else if (page === 'recov') {
		reqBody = {
			email: flag.email
		}
	}
	return reqBody
}
export const getReqMethod = (method, reqBody, headers, flag = '') => {
	let reqMethod
	if (method === 'post') {
		reqMethod = {
			method: 'POST',
			body: JSON.stringify(reqBody),
			headers: headers,
			mode: 'cors',
			cache: 'default'
		}
	} else {
		reqMethod = {
			method: 'GET',
			headers: headers,
			mode: 'cors',
			cache: 'default'
		}
	}

	return reqMethod
}

export const apiRequest = async (request, headers, method, page, flag = '') => {
	const reqBody = getReqBody(page, flag)
	const reqMethod = getReqMethod(method, reqBody, headers)

	console.log(request, reqMethod)
	const requestStr = new Request(request, reqMethod)
	return new Promise(function(resolve, reject) {
		fetch(requestStr)
			.then((response) => {
				if (response.ok) {
					const contentType = response.headers.get('content-type')
					//console.log(contentType)
					if (
						(contentType && contentType.indexOf('application/json') !== -1) ||
						(contentType && contentType.indexOf('text/plain') !== -1)
					) {
						return response.json()
					} else {
						if (page === 'recov') {
							const responseRecov = {
								message: ' Successful'
							}
							resolve(responseRecov)
							return false
						} else {
							console.log(`${request}  - Error ( response NOT application/json )`)
							reject()
							return false
						}
					}
				} else {
					if (page === 'auth') {
						const responseRecov = {
							message: 'User not found!'
						}
						reject(responseRecov)
					} else {
						console.log(`${request}  - Unsuccessful (response NOT response.ok)`)
						reject(response)
						return false
					}
				}
			})
			.then((data) => {
				console.log(`${request}  - OK`)

				if (flag === 'offersstatus') {
					for (let i = 0; i < data.offersByStatus.length; i++) {
						if (data.offersByStatus[i].showProbability === 'H') {
							data.offersByStatus[i].showProbability = data.offersByStatus[i].showProbability.replace(
								'H',
								'On Track'
							)
						} else if (data.offersByStatus[i].showProbability === 'W') {
							data.offersByStatus[i].showProbability = data.offersByStatus[i].showProbability.replace(
								'W',
								'Erratic'
							)
						} else if (data.offersByStatus[i].showProbability === 'C') {
							data.offersByStatus[i].showProbability = data.offersByStatus[i].showProbability.replace(
								'C',
								'On Thin Ice'
							)
						} else {
							data.offersByStatus[i].showProbability = 'WIP'
						}
					}
					console.log(data)
					resolve(data)
					return false
				} else if (flag === 'offersbymontstatus') {
					for (let i = 0; i < data.offersByMonthStatus.length; i++) {
						if (data.offersByMonthStatus[i].key === 'H') {
							data.offersByMonthStatus[i].key = data.offersByMonthStatus[i].key.replace('H', 'On Track')
						} else if (data.offersByMonthStatus[i].key === 'W') {
							data.offersByMonthStatus[i].key = data.offersByMonthStatus[i].key.replace('W', 'Erratic')
						} else if (data.offersByMonthStatus[i].key === 'C') {
							data.offersByMonthStatus[i].key = data.offersByMonthStatus[i].key.replace('C', 'On Thin Ice')
						} else {
							data.offersByMonthStatus[i].showProbability = 'WIP'
						}
					}
					resolve(data)
					return false
				}

				resolve(data)
			})
			.catch(function(error) {
				console.log(`${request}  - Error`)
				console.log(error)
				reject(error)
			})
	})
}
