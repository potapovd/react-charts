import React from 'react'
import cookie from 'react-cookies'

//LOGOUT Function
export const logOut = () => {
	console.log('Loging out...')
	cookie.remove('userToken', {
		path: '/'
	})
	cookie.remove('userId', {
		path: '/'
	})
	cookie.remove('userName', {
		path: '/'
	})
	cookie.remove('companyId', {
		path: '/'
	})
	document.location = '/'
	return false
}

//Check if User Logged in
export const userIsLogged = () => {
	let userToken = cookie.load('userToken')
	let userId = cookie.load('userId')
	let userName = cookie.load('userName')
	let companyId = cookie.load('companyId')
	if (
		typeof userToken !== 'undefined' &&
		typeof userId !== 'undefined' &&
		typeof userName !== 'undefined' &&
		typeof companyId !== 'undefined'
	) {
		return userToken
	} else {
		logOut()
	}
}

export const getHeadersXAuthToken = (userToken) => {
	if (typeof userToken !== 'undefined') {
		const reqHeaders = new Headers({
			'Content-Type': 'application/json',
			'X-Auth-Token': userToken
		})
		return reqHeaders
	} else {
		logOut()
	}
}
