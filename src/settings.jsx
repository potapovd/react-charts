const baseHost = 'http://rockethire.org:8080'
export const APISCHORT = `${baseHost}/recruitment/api/`
export const API = `${APISCHORT}resources/`

export const COLOR_RED = '#d62728'
export const COLOR_GREEN = '#2ca02c'
export const COLOR_YELLOW = '#ff7f0e'
