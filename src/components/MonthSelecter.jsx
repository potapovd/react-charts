import React from 'react'
import moment from 'moment'

export const getPeriod = (period, month = 'Oct-Dec') => {
	let endOfPriod1 = moment(new Date().getFullYear() + '-03-30')
	let endOfPriod2 = moment(new Date().getFullYear() + '-06-30')
	let endOfPriod3 = moment(new Date().getFullYear() + '-09-30')
	let endOfPriod4 = moment(new Date().getFullYear() + '-12-31')
	let dataArr
	if (period.isAfter(endOfPriod4)) {
		dataArr = [
			'01-10-' + new Date().getFullYear(),
			'31-12-' + new Date().getFullYear(),
			new Date().getFullYear()
		]
	} else if (period.isAfter(endOfPriod3)) {
		dataArr = [
			'01-07-' + new Date().getFullYear(),
			'30-09-' + new Date().getFullYear(),
			new Date().getFullYear()
		]
	} else if (period.isAfter(endOfPriod2)) {
		dataArr = [
			'01-04-' + new Date().getFullYear(),
			'30-06-' + new Date().getFullYear(),
			new Date().getFullYear()
		]
	} else if (period.isAfter(endOfPriod1)) {
		dataArr = [
			'01-01-' + new Date().getFullYear(),
			'30-03-' + new Date().getFullYear(),
			new Date().getFullYear()
		]
	} else {
		console.log(month)
		if (month === 'Jan-Mar') {
			dataArr = [
				'01-01-' + (new Date().getFullYear() - 1),
				'30-03-' + (new Date().getFullYear() - 1),
				new Date().getFullYear() - 1
			]
		}
		if (month === 'Apr-Jun') {
			dataArr = [
				'01-04-' + (new Date().getFullYear() - 1),
				'30-06-' + (new Date().getFullYear() - 1),
				new Date().getFullYear() - 1
			]
		}
		if (month === 'Jul-Sep') {
			dataArr = [
				'01-07-' + (new Date().getFullYear() - 1),
				'30-09-' + (new Date().getFullYear() - 1),
				new Date().getFullYear() - 1
			]
		}
		if (month === 'Oct-Dec') {
			dataArr = [
				'01-10-' + (new Date().getFullYear() - 1),
				'31-12-' + (new Date().getFullYear() - 1),
				new Date().getFullYear() - 1
			]
		}
	}
	dataArr.push(month)
	console.log('Period ' + dataArr)
	return dataArr
}
export class MonthSelecter extends React.Component {
	handleChange(e) {
		this.props.changePeriond(e.target.value)
	}
	render() {
		return (
			<div className="col-md-12 col-lg-6 offset-lg-3">
				<div className="card">
					<div className="card-block" id="multiBarChartDiv">
						<div className="form-group row">
							<div className="col-sm-12">
								<select
									name="select"
									className="form-control"
									onChange={this.handleChange.bind(this)}
									value={this.props.defaulPeriod}>
									<option value="Jan-Mar">Jan - Mar</option>
									<option value="Apr-Jun">Apr - Jun</option>
									<option value="Jul-Sep">Jul - Sep</option>
									<option value="Oct-Dec">Oct - Dec</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
