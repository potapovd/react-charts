import React from 'react'
import Moment from 'react-moment'
let moment = require('moment')

class OneUserModal extends React.Component {
	render() {
		let newTimelineData = this.props.timelineData.sort(function(a, b) {
			return parseFloat(a.scheduledFor) - parseFloat(b.scheduledFor)
		})
		//horizontalTimelineRunner
		return (
			<div className="modal fade modal-flex" id="large-Modal-OneUser" tabIndex={-1} role="dialog">
				<div className="modal-dialog modal-lg" role="document">
					<div className="modal-content">
						<div className="modal-header">
							<h4 className="modal-title">TimeLine</h4>
							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div className="modal-body">
							<div className="col-md-12">
								<div className="card">
									<div className="card-block">
										{/* Horizontal Timeline start */}
										<div className="cd-horizontal-timeline">
											<div className="timeline">
												<div className="events-wrapper">
													<div className="events" id="foo">
														<ol>
															{newTimelineData.map((item, index) => (
																<li key={item.id}>
																	<a
																		href="#0"
																		data-date={moment(item.scheduledFor).format('DD/MM/YYYY')}
																		className={index === 0 ? 'selected' : null}>
																		<Moment unix format="DD MMM">
																			{item.scheduledFor / 1000}
																		</Moment>
																	</a>
																</li>
															))}
														</ol>
														<span className="filling-line" aria-hidden="true" />
													</div>
													{/* .events */}
												</div>
												{/* .events-wrapper */}
												<ul className="cd-timeline-navigation">
													<li>
														<a href="#0" className="prev inactive">
															Prev
														</a>
													</li>
													<li>
														<a href="#0" className="next">
															Next
														</a>
													</li>
												</ul>
												{/* .cd-timeline-navigation */}
											</div>
											{/* .timeline */}
											<div className="events-content">
												<ol>
													{newTimelineData.map((item, index) => (
														<li
															key={item.id}
															className={index === 0 ? 'selected' : null}
															data-date={moment(item.scheduledFor).format('DD/MM/YYYY')}>
															<div className="row">
																<div className="col-sm-8">
																	<h2>{item.candidateName}</h2>
																</div>
																<div className="col-sm-4 text-center">
																	<i>{item.status}</i>
																</div>
															</div>
															<div className="row">
																<div className="col-sm-8">{item.jobName}</div>
																<div className="col-sm-4">joing date</div>
															</div>
															<div className="row">
																<div className="col-sm-8">{item.sla}</div>
																<div className="col-sm-4">offered date</div>
															</div>
															<p className="m-b-0">{item.comments}</p>
														</li>
													))}
												</ol>
											</div>
											{/* .events-content */}
										</div>

										{/* Horizontal Timeline end */}
									</div>
								</div>
							</div>
						</div>
						<div className="modal-footer">
							<button
								type="button"
								className="btn btn-primary waves-effect waves-light"
								data-dismiss="modal">
								Close
							</button>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default OneUserModal
