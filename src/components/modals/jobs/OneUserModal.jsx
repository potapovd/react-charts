import React from 'react'
import Moment from 'react-moment'

class JobTitleModalRejectedDataLine extends React.Component {
	constructor(props) {
		super(props)
		this.handleClick = this.handleClick.bind(this)
	}
	handleClick() {
		this.props.onClick(this.props.data.candidate.id)
	}
	render() {
		return (
			<tr className="text-capitalize">
				<td>{this.props.data.candidate.profileUploadPath}</td>
				<td>
					<a data-toggle="modal" href="#large-Modal-OneUser" onClick={this.handleClick}>
						{this.props.data.candidate.firstName}&nbsp; {this.props.data.candidate.lastName}
						{this.props.data.candidate.id}
					</a>
				</td>
				<td>{this.props.data.candidate.gender}</td>
				<td>{this.props.data.candidate.city}</td>
				<td>{this.props.data.candidate.email}</td>
				<td>{this.props.data.candidate.mobile}</td>
				<td>
					{this.props.data.candidate.currentComp
						? this.props.data.candidate.currentComp.companyName
						: ''}
				</td>
				<td>
					{this.props.data.candidate.currentComp
						? this.props.data.candidate.currentComp.designation
						: ''}
				</td>
				<td>
					{this.props.data.candidate.currentComp ? this.props.data.candidate.currentComp.expFrom : ''}
				</td>
				<td>{this.props.data.candidate.yearsOfExp}</td>
				<td>
					{this.props.data.candidate.currentComp
						? this.props.data.candidate.currentComp.noticePeriod
						: ''}
				</td>
			</tr>
		)
	}
}

class OneUserModalLine extends React.Component {
	render() {
		let {changedOn, stage, step, timelineActionInfo} = this.props.data
		let createdOnNorm = changedOn / 1000
		return (
			<div className="cd-timeline-block">
				<div className="cd-timeline-icon bg-success">
					<i className="icofont icofont-ui-user" />
				</div>
				<div className="cd-timeline-content card_main">
					<div className="media bg-white d-flex p-10 d-block-phone">
						<div className="media-body">
							<div className="f-15 f-bold m-b-5">{stage}</div>
							<div className="f-13 text-muted">
								{step}
								<br />
								{timelineActionInfo}
							</div>
						</div>
					</div>
					<span className="cd-date">
						<Moment unix format="DD-MM-YYYY HH:mm">
							{createdOnNorm}
						</Moment>
					</span>
				</div>
			</div>
		)
	}
}
class OneUserModal extends React.Component {
	render() {
		return (
			<div className="modal fade modal-flex" id="large-Modal-OneUser" tabIndex={-1} role="dialog">
				<div className="modal-dialog modal-lg" role="document">
					<div className="modal-content">
						<div className="modal-header">
							<h4 className="modal-title">{this.props.userid}</h4>
							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div className="modal-body">
							<div className="card">
								<div className="card-block">
									<div className="main-timeline">
										<div className="cd-timeline cd-container">
											{this.props.data.map((item, index) => <OneUserModalLine key={index} data={item} />)}
										</div>
										{/* cd-timeline */}
									</div>
								</div>
							</div>
						</div>
						<div className="modal-footer">
							<button
								type="button"
								className="btn btn-primary waves-effect waves-light"
								data-dismiss="modal">
								Close
							</button>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
export default OneUserModal
