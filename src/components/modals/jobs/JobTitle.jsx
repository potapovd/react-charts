import React from 'react'
import Moment from 'react-moment'

class JobTitleModalRejectedDataLine extends React.Component {
	constructor(props) {
		super(props)
		this.handleClick = this.handleClick.bind(this)
	}
	handleClick() {
		this.props.onClick(this.props.data.candidate.id)
	}
	render() {
		let currentCompExpFrom = this.props.data.candidate.currentComp.expFrom / 1000
		return (
			<tr className="text-capitalize">
				<td>{this.props.data.candidate.profileUploadPath}</td>
				<td>
					<a data-toggle="modal" href="#large-Modal-OneUser" onClick={this.handleClick}>
						{this.props.data.candidate.firstName}&nbsp; {this.props.data.candidate.lastName}
						{this.props.data.candidate.id}
					</a>
				</td>
				<td>{this.props.data.candidate.gender}</td>
				<td>{this.props.data.candidate.city}</td>
				<td>{this.props.data.candidate.email}</td>
				<td>{this.props.data.candidate.mobile}</td>
				<td>
					{this.props.data.candidate.currentComp
						? this.props.data.candidate.currentComp.companyName
						: ''}
				</td>
				<td>
					{this.props.data.candidate.currentComp
						? this.props.data.candidate.currentComp.designation
						: ''}
				</td>
				<td>
					{this.props.data.candidate.currentComp ? (
						<Moment unix format="DD-MM-YYYY">
							{currentCompExpFrom}
						</Moment>
					) : (
						''
					)}
				</td>
				<td>{this.props.data.candidate.yearsOfExp}</td>
				<td>
					{this.props.data.candidate.currentComp
						? this.props.data.candidate.currentComp.noticePeriod
						: ''}
				</td>
			</tr>
		)
	}
}
class OneLineJobTitleModal extends React.Component {
	constructor(props) {
		super(props)
		this.handleClick = this.handleClick.bind(this)
	}

	handleClick() {
		this.props.onClick(this.props.data.candidate.id)
	}
	render() {
		let currentCompExpFrom = this.props.data.candidate.currentComp.expFrom / 1000
		return (
			<tr className="text-capitalize">
				<td>{this.props.data.candidate.profileUploadPath}</td>
				<td>
					<a data-toggle="modal" href="#large-Modal-OneUser" onClick={this.handleClick}>
						{this.props.data.candidate.firstName}&nbsp;
						{this.props.data.candidate.lastName}
					</a>
				</td>
				<td>{this.props.data.candidate.gender}</td>
				<td>{this.props.data.candidate.city}</td>
				<td>{this.props.data.candidate.email}</td>
				<td>{this.props.data.candidate.mobile}</td>
				<td>{this.props.data.candidate.currentComp.companyName}</td>
				<td>{this.props.data.candidate.currentComp.designation}</td>
				<td>
					<Moment unix format="DD-MM-YYYY">
						{currentCompExpFrom}
					</Moment>
				</td>
				<td>{this.props.data.candidate.yearsOfExp}</td>
				<td>{this.props.data.candidate.currentComp.noticePeriod}</td>
			</tr>
		)
	}
}
class JobTitleModal extends React.Component {
	render() {
		return (
			<div>
				<div className="modal fade modal-flex" id="large-Modal-title-job" tabIndex={-1} role="dialog">
					<div className="modal-dialog modal-lg" role="document">
						<div className="modal-content">
							<div className="modal-header">
								<h4 className="modal-title">{this.props.dataId}</h4>
								<button type="button" className="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
							<div className="modal-body">
								<div className="card">
									<div className="card-block">
										<ul className="nav nav-tabs" role="tablist">
											<li className="nav-item">
												<a className="nav-link active" data-toggle="tab" href="#tab1" role="tab">
													Live Candidates
												</a>
											</li>
											<li className="nav-item">
												<a
													className="nav-link"
													data-toggle="tab"
													href="#tab2"
													role="tab"
													onClick={this.props.hadleOpenJobTitleModalRejectedData}>
													Rejected Candidates
												</a>
											</li>
										</ul>
										<div className="tab-content modal-body">
											<div className="tab-pane active" id="tab1" role="tabpanel">
												<div className="table-responsive">
													<table className="table">
														<thead>
															<tr className="text-capitalize">
																<th>Link to resume</th>
																<th>Name</th>
																<th>Gender</th>
																<th>Location</th>
																<th>Email</th>
																<th>Mobile</th>
																<th>Current Company</th>
																<th>Current Company Designation</th>
																<th>Current company start date</th>
																<th>Years of exp</th>
																<th>Notice Period</th>
															</tr>
														</thead>
														<tbody>
															{this.props.data.map((item) => (
																<OneLineJobTitleModal key={item.id} data={item} onClick={this.props.onClick} />
															))}
														</tbody>
													</table>
												</div>
											</div>
											<div className="tab-pane" id="tab2" role="tabpanel">
												<div className="tab-pane active" id="tab1" role="tabpanel">
													<div className="table-responsive">
														<table className="table">
															<thead>
																<tr className="text-capitalize">
																	<th>Link to resume</th>
																	<th>Name</th>
																	<th>Gender</th>
																	<th>Location</th>
																	<th>Email</th>
																	<th>Mobile</th>
																	<th>Current Company</th>
																	<th>Current Company Designation</th>
																	<th>Current company start date</th>
																	<th>Years of exp</th>
																	<th>Notice Period</th>
																</tr>
															</thead>
															<tbody>
																{this.props.dataModalRejectedData.map((item) => (
																	<JobTitleModalRejectedDataLine
																		key={item.id}
																		data={item}
																		onClick={this.props.onClick}
																	/>
																))}
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="modal-footer">
								<button
									type="button"
									className="btn btn-primary waves-effect waves-light"
									data-dismiss="modal">
									Close
								</button>
							</div>
						</div>
					</div>
				</div>

				<div />
			</div>
		)
	}
}
export default JobTitleModal
