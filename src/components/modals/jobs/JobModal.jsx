import React from 'react'
import Moment from 'react-moment'

class JobModal extends React.Component {
	render() {
		let {
			id,
			designation,
			createdOn,
			clientSPOC,
			location,
			noOfPositions,
			minExp,
			maxExp,
			drive,
			hiringManagerName,
			jobDescription
		} = this.props.data

		let employerCompanyBuName
		if (this.props.data.employerCompanyBu !== undefined) {
			employerCompanyBuName = this.props.data.employerCompanyBu.name
			//console.log(employerCompanyBuName);
		}

		let jobDescriptionClear
		if (this.props.data.jobDescription !== undefined) {
			//console.log(jobDescription);
			jobDescriptionClear = jobDescription.replace(/&nbsp;/g, ' ').replace(/<\/?[^>]+(>|$)/g, '')
		}

		let createdOnNorm
		if (Number.isInteger(createdOn)) {
			createdOnNorm = createdOn / 1000
		}

		return (
			<div>
				<div className="modal fade" id="large-Modal-job" tabIndex={-1} role="dialog">
					<div className="modal-dialog modal-lg" role="document">
						<div className="modal-content">
							<div className="modal-header">
								<h4 className="modal-title">{id}</h4>
								<button type="button" className="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
							<div className="modal-body">
								<div className="card">
									<div className="card-block">
										<h4 className="sub-title">Job Designation</h4>
										<p>{designation}</p>

										<h4 className="sub-title">Created On</h4>
										<p>
											{createdOn ? (
												<Moment unix format="DD-MM-YYYY">
													{createdOnNorm}
												</Moment>
											) : (
												''
											)}
										</p>

										<h4 className="sub-title">BU</h4>
										<p>{employerCompanyBuName}</p>

										<h4 className="sub-title">Client SPOC</h4>
										<p>{clientSPOC}</p>

										<h4 className="sub-title">Location</h4>
										<p>{location}</p>

										<h4 className="sub-title">#of Positions</h4>
										<p>{noOfPositions}</p>

										<h4 className="sub-title">Min-Max exp</h4>
										<p>
											{minExp}-{maxExp}
										</p>

										<h4 className="sub-title">Drive</h4>
										<p>{drive}</p>

										<h4 className="sub-title">Hiring Manager</h4>
										<p>{hiringManagerName}</p>

										<h4 className="sub-title">Description</h4>
										<p>{jobDescriptionClear}</p>
									</div>
								</div>
							</div>
							<div className="modal-footer">
								<button
									type="button"
									className="btn btn-primary waves-effect waves-light"
									data-dismiss="modal">
									Close
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
export default JobModal
