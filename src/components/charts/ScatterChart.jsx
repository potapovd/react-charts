import React from 'react'
import NVD3Chart from 'react-nvd3'

class ScatterChart extends React.Component {
	render() {
		return (
			<NVD3Chart
				type="scatterChart"
				width={360}
				height={320}
				showValues={true}
				showControls={false}
				showLegend={true}
				datum={this.props.data}
				x={this.props.x}
				y={this.props.y}
			/>
		)
	}
}
export default class ScatterChartDiv extends React.Component {
	render() {
		return (
			<div className={`${this.props.blockSize}`}>
				<div className="card">
					<div className="card-header">
						<h5>{this.props.chartName}</h5>
						<span>{this.props.chartData}</span>
					</div>
					<div className="card-block">
						<ScatterChart datum={this.props.chartData} x={this.props.chartX} y={this.props.chartY} />
					</div>
				</div>
			</div>
		)
	}
}
