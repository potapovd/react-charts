import React from 'react'
import NVD3Chart from 'react-nvd3'

class MultiBarChart extends React.Component {
	render() {
		return (
			<NVD3Chart
				type="multiBarChart"
				width={document.body.clientWidth - document.body.clientWidth * 0.13}
				height={520}
				stacked={true}
				showControls={false}
				color={this.props.color}
				datum={this.props.datum}
				x={this.props.x}
				y={this.props.y}
			/>
		)
	}
}
export default class MultiBarChartDiv extends React.Component {
	render() {
		return (
			<div className={`${this.props.blockSize}`}>
				<div className="card">
					<div className="card-header">
						<h5>{this.props.chartName}</h5>
						<span />
					</div>
					<div className="card-block">
						<MultiBarChart
							datum={this.props.chartData}
							x={this.props.chartX}
							y={this.props.chartY}
							color={this.props.chartColor}
						/>
					</div>
				</div>
			</div>
		)
	}
}
