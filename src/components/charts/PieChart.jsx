import React from 'react'
import NVD3Chart from 'react-nvd3'

class PieChart extends React.Component {
	render() {
		return (
			<NVD3Chart
				type="pieChart"
				width={360}
				height={320}
				showLabel={false}
				showControls={false}
				showLegend={false}
				labelType="percent"
				donutRatio={0.35}
				donut={true}
				datum={this.props.datum}
				x={this.props.x}
				y={this.props.y}
			/>
		)
	}
}
export default class PieChartDiv extends React.Component {
	render() {
		return (
			<div className={`${this.props.blockSize}`}>
				<div className="card">
					<div className="card-header">
						<h5>{this.props.chartName}</h5>
						<span />
					</div>
					<div className="card-block">
						<PieChart datum={this.props.chartData} x={this.props.chartX} y={this.props.chartY} />
					</div>
				</div>
			</div>
		)
	}
}
