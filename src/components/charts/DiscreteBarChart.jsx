import React from 'react'
import NVD3Chart from 'react-nvd3'

class DiscreteBarChart extends React.Component {
	render() {
		return (
			<NVD3Chart
				type="discreteBarChart"
				width={document.body.clientWidth / 2 * 1 - document.body.clientWidth * 0.1}
				height={320}
				showValues={true}
				showControls={false}
				showLegend={false}
				staggerLabels={true}
				showLabel={false}
				datum={this.props.datum}
				x={this.props.x}
				y={this.props.y}
			/>
		)
	}
}
export default class DiscreteBarChartDiv extends React.Component {
	render() {
		return (
			<div className={`${this.props.blockSize}`}>
				<div className="card">
					<div className="card-header">
						<h5>{this.props.chartName}</h5>
						<span />
					</div>
					<div className="card-block" id="discreteBarChartOut">
						<DiscreteBarChart datum={this.props.chartData} x={this.props.chartX} y={this.props.chartY} />
					</div>
				</div>
			</div>
		)
	}
}
