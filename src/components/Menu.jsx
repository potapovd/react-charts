import React from 'react'
import cookie from 'react-cookies'
import {Link} from 'react-router-dom'
import {logOut} from '../actions/CheckUserAuth.jsx'

import LogoImg from '../assets/images/logo-hexagon-whitebg.png'
import UserImg from '../assets/images/user.png'

export default class Menu extends React.Component {
	handelLogout(e) {
		e.preventDefault()
		logOut()
	}
	render() {
		return (
			<div>
				<div className="theme-loader">
					<div className="ball-scale">
						<div />
					</div>
				</div>
				<nav className="navbar navbar-expand-lg navbar-light navbar-dark dark-nav-border bg-dark">
					<a className="navbar-brand" href="">
						<img className="img-40" src={LogoImg} alt="" />
						<span>HEXAGON</span>
					</a>
					<button
						className="navbar-toggler"
						type="button"
						data-toggle="collapse"
						data-target="#navbar-white"
						aria-controls="navbar-white"
						aria-expanded="false"
						aria-label="Toggle navigation">
						<span className="navbar-toggler-icon" />
					</button>
					<div className="collapse navbar-collapse" id="navbar-white">
						<ul className="navbar-nav mr-auto">
							<li className="nav-item dropdown">
								<a className="nav-link" href="">
									Jobs <i className="icofont icofont-rounded-down" />
								</a>
								<ul className="navbar-varient-submenu">
									<li>
										<Link to="jobs-dashboard">Dashboard</Link>
									</li>
									<li>
										<Link to="jobs-tracker">Tracker</Link>
									</li>
								</ul>
							</li>
							<li className="nav-item dropdown">
								<a className="nav-link" href="">
									Offers <i className="icofont icofont-rounded-down" />
								</a>
								<ul className="navbar-varient-submenu">
									<li>
										<Link to="offers-dashboard">Dashboard</Link>
									</li>
									<li>
										<Link to="offers-tracker">Tracker</Link>
									</li>
								</ul>
							</li>
							<li className="nav-item dropdown">
								<a className="nav-link" href="">
									Analytics <i className="icofont icofont-rounded-down" />
								</a>
								<ul className="navbar-varient-submenu">
									<li>
										<Link to="analytics-time-and-throughput">Time &amp; Throughput Metrics</Link>
									</li>
									<li>
										<Link to="analytics-reasons-for-rejection">Reasons for Rejection</Link>
									</li>
									<li>
										<Link to="analytics-show-analysis">IV Show Analysis</Link>
									</li>
									<li>
										<Link to="analytics-joined-candidate">Joined Candidate Analysis</Link>
									</li>
								</ul>
							</li>
						</ul>
						<form className="form-inline">
							<div className="nav-item">
								<a href="">
									<img className="img-40" src={UserImg} alt="" />
									<span className="m-l-10" />
									<i className="icofont icofont-rounded-down" />
								</a>
								<ul className="navbar-varient-submenu profile-sub-menu">
									<li>
										<a href="">
											<i className="icon-user" /> {cookie.load('userName')}
										</a>
									</li>
									<li>
										<a href="" onClick={this.handelLogout.bind(this)}>
											<i className="icon-logout" /> Logout
										</a>
									</li>
								</ul>
							</div>
						</form>
					</div>
				</nav>
			</div>
		)
	}
}
