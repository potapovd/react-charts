import React from 'react'
import Moment from 'react-moment'
import * as SETTINGS from '../settings.jsx'
import {userIsLogged, getHeadersXAuthToken} from '../actions/CheckUserAuth.jsx'
import {apiRequest} from '../actions/APIRequests.jsx'
import Menu from '../components/Menu.jsx'

//modals info
import JobModal from '../components/modals/jobs/JobModal.jsx'
import JobTitleModal from '../components/modals/jobs/JobTitle.jsx'
import OneUserModal from '../components/modals/jobs/OneUserModal.jsx'

//table and popdals scripts
import '../assets/css/dataTables.bootstrap4.min.css'
import $ from 'jquery'
let jQuery = window.jQuery  = window.$ = $
$.DataTable = require('datatables.net')
require('datatables.net-bs4')
require('popper.js')
require('bootstrap')

class OnelineTrackerJob extends React.Component {
	render() {
		let createdOn
		if (Number.isInteger(this.props.tabdata.createdOn)) {
			createdOn = this.props.tabdata.createdOn / 1000
		}

		let className = this.props.activeLine === this.props.lineId ? 'active' : ''
		return (
			<tr
				className={className}
				onClick={this.props.handelChangeActiveLine.bind(this, this.props.tabdata.id)}>
				<td>
					<a
						className="waves-effect"
						data-toggle="modal"
						data-target="#large-Modal-job"
						onClick={this.props.handelOpenJobModal.bind(this, this.props.tabdata.id)}>
						{this.props.tabdata.id}
					</a>
				</td>
				<td>
					<a
						className="waves-effect"
						data-toggle="modal"
						data-target="#large-Modal-title-job"
						onClick={this.props.handelOpenJobTitleModal.bind(this, this.props.tabdata.id)}>
						{this.props.tabdata.designation}
					</a>
				</td>
				<td>{this.props.tabdata.corporateTitle}</td>
				<td>{this.props.tabdata.employerCompanyBu.name}</td>
				<td>{this.props.tabdata.clientSPOC}</td>
				<td>{this.props.tabdata.noOfPositions}</td>
				<td>
					{createdOn ? (
						<Moment unix format="DD-MM-YYYY">
							{createdOn}
						</Moment>
					) : (
						''
					)}
				</td>
				<td>{this.props.tabdata.jobMetric.metricCVSubmittedCandidatesCount}</td>
				<td>{this.props.tabdata.jobMetric.metricInterviewCandidatesCount}</td>
				<td>{this.props.tabdata.jobMetric.metricSTOCandidatesCount}</td>
				<td>{this.props.tabdata.jobMetric.metricOfferAcceptedCount}</td>
				<td>{this.props.tabdata.jobMetric.metricJoiningCandidatesCount}</td>
			</tr>
		)
	}
}

class JobsTracker extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			tabdata: [],
			activeLine: '',
			activeJobId: '',
			activeJobData: [],
			activeJobTitleLive: [],
			activeJobTitleRejected: [],
			activeOneUser: '',
			activeOneUserTimeLine: []
		}
		this.handelChangeActiveLine = this.handelChangeActiveLine.bind(this)
		this.handelJobModal = this.handelJobModal.bind(this)
		this.handelJobTitleModal = this.handelJobTitleModal.bind(this)
		this.handelOneUserModal = this.handelOneUserModal.bind(this)
		this.handelJobTitleModalRejectedData = this.handelJobTitleModalRejectedData.bind(this)
	}

	handelChangeActiveLine(event) {
		this.setState({activeLine: event})
	}

	handelJobModal(event) {
		this.setState(
			{
				activeJobId: event
			},
			() => {
				let userToken = userIsLogged()
				const reqHeaders = getHeadersXAuthToken(userToken)
				const apiURL = `${SETTINGS.API}jobs/${event}/1`
				apiRequest(apiURL, reqHeaders, 'get', 'jobs').then(
					function(response) {
						this.setState({activeJobData: response})
					}.bind(this)
				)
			}
		)
	}

	handelOneUserModal(event) {
		this.setState(
			{
				activeOneUser: event
			},
			() => {
				let userToken = userIsLogged()
				const reqHeaders = getHeadersXAuthToken(userToken)
				const apiURL = `${SETTINGS.API}cnjobprocess/timeline`
				let flag = {
					activeJobId: this.state.activeJobId,
					activeOneUser: this.state.activeOneUser
				}
				apiRequest(apiURL, reqHeaders, 'post', 'jobs-tracker-modal-oneUser', flag).then(
					function(response) {
						this.setState({activeOneUserTimeLine: response})
					}.bind(this)
				)
			}
		)
	}

	handelJobTitleModal(event) {
		this.setState(
			{
				activeJobId: event
			},
			() => {
				let userToken = userIsLogged()
				const reqHeaders = getHeadersXAuthToken(userToken)
				const apiURL = `${SETTINGS.API}cnjobprocess/filter`
				apiRequest(
					apiURL,
					reqHeaders,
					'post',
					'jobs-tracker-modal-jobTitle',
					this.state.activeJobId
				).then(
					function(response) {
						this.setState({activeJobTitleLive: response})
					}.bind(this)
				)
			}
		)
	}

	handelJobTitleModalRejectedData(event) {
		let userToken = userIsLogged()
		const reqHeaders = getHeadersXAuthToken(userToken)
		const apiURL = `${SETTINGS.API}cnjobprocess/jobs/${this.state.activeJobId}/1`
		apiRequest(apiURL, reqHeaders, 'get', 'jobs').then(
			function(response) {
				this.setState({activeJobTitleRejected: response})
			}.bind(this)
		)
	}

	componentDidMount() {
		let userToken = userIsLogged()
		const reqHeaders = getHeadersXAuthToken(userToken)
		const apiURL = `${SETTINGS.API}jobs/all/1`
		apiRequest(apiURL, reqHeaders, 'get', 'jobs').then(
			function(response) {
				this.setState(
					{
						tabdata: response
					},
					() => {
						jQuery(this.tableSmart).DataTable()
						//$(this.refs.tableSmart).DataTable();
						//jQuery("#tableSmart").DataTable();
					}
				)
			}.bind(this)
		)
	}

	render() {
		return (
			<div>
				<Menu />

				<div className="main-body">
					<div className="page-wrapper">
						<div className="page-header">
							<div className="page-header-title">
								<h4>Jobs - Tracker</h4>
							</div>
						</div>
						<div className="page-body">
							<div className="row">
								{/* User detail table start */}
								<div className="col-xl-12">
									<div className="row">
										<div className="col-sm-12">
											<div className="card table-1-card">
												<div className="card-block">
													<div className="table-responsive">
														<table
															className="table"
															id="tableSmart"
															ref={(tableSmart) => (this.tableSmart = tableSmart)}>
															<thead>
																<tr className="text-capitalize">
																	<th>PCN</th>
																	<th>Job Title</th>
																	<th>Corporate Title</th>
																	<th>BU</th>
																	<th>HM/SPOC</th>
																	<th># of Positions</th>
																	<th>Created On</th>
																	<th>CV Ct</th>
																	<th>IV Ct</th>
																	<th>STO Ct</th>
																	<th>Offered Ct</th>
																	<th>Joined Ct</th>
																</tr>
															</thead>
															<tbody>
																{this.state.tabdata.map((tabOneLine) => (
																	<OnelineTrackerJob
																		key={tabOneLine.id}
																		tabdata={tabOneLine}
																		activeLine={this.state.activeLine}
																		lineId={tabOneLine.id}
																		handelChangeActiveLine={this.handelChangeActiveLine.bind(this)}
																		handelOpenJobModal={this.handelJobModal.bind(this)}
																		handelOpenJobTitleModal={this.handelJobTitleModal.bind(this)}
																	/>
																))}
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								{/* User detail table end */}
							</div>
						</div>
					</div>
				</div>
				<JobModal data={this.state.activeJobData} />
				<JobTitleModal
					data={this.state.activeJobTitleLive}
					dataModalRejectedData={this.state.activeJobTitleRejected}
					dataId={this.state.activeJobId}
					hadleOpenJobTitleModalRejectedData={this.handelJobTitleModalRejectedData.bind(this)}
					onClick={this.handelOneUserModal.bind(this)}
				/>

				<OneUserModal data={this.state.activeOneUserTimeLine} userid={this.state.activeOneUser} />
			</div>
		)
	}
}

export default JobsTracker
