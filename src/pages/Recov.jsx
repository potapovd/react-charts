import React from 'react'
import cookie from 'react-cookies'
import {Link} from 'react-router-dom'
import * as SETTINGS from '../settings.jsx'
import {apiRequest} from '../actions/APIRequests.jsx'

import LogoImg from '../assets/images/logo-hexagon-whitebg.png'

class SuccessfulMessage extends React.Component {
	render() {
		return (
			<div className="alert alert-info background-info hiddenalerts">
				<strong>{this.props.textmessage}</strong>
			</div>
		)
	}
}
class ErrorMessage extends React.Component {
	render() {
		return (
			<div className="alert alert-danger background-danger hiddenalerts">
				<strong>{this.props.textmessage}</strong>
			</div>
		)
	}
}

class Recov extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			email: '',
			msgtxt: '',
			msgerror: false,
			msgsucc: false
		}
		this.handleChange = this.handleChange.bind(this)
		this.handleReset = this.handleReset.bind(this)
	}

	handleChange({target}) {
		this.setState({[target.name]: target.value})
	}

	handleReset() {
		if (this.state.email.length > 2) {
			this.setState({msgerror: false})
			this.setState({msgsucc: false})

			const authHeader = new Headers({
				'Content-Type': 'application/json'
			})
			let authFlag = {email: this.state.email}
			const apiURL = `${SETTINGS.APISCHORT}auth/forgotpassword`
			apiRequest(apiURL, authHeader, 'post', 'recov', authFlag).then(
				function(response) {
					console.log('RequestReset - Successful ', response)
					this.setState({msgsucc: true, msgtxt: response.message})
				}.bind(this)
			)
		}
	}

	componentDidMount() {
		if (cookie.load('userToken')) {
			document.location = '/jobs-dashboard'
		}
	}

	render() {
		return (
			<div>
				<section className="login p-fixed d-flex text-center bg-primary common-img-bg">
					{/* Container-fluid starts */}
					<div className="container-fluid">
						<div className="row">
							<div className="col-sm-12">
								{/* Authentication card start */}
								<div className="login-card card-block auth-body">
									<form className="md-float-material">
										<div className="text-center">
											<img src={LogoImg} alt="logo-hexagon" />
											<h1>HEXAGON</h1>
										</div>
										<div className="auth-box">
											<div className="row m-b-20">
												<div className="col-md-12">
													<h3 className="text-left">You forgot your Password? </h3>
													<h3 className="text-left">Don't worry.</h3>
												</div>
											</div>
											<p className="text-inverse b-b-default text-right">
												Back to &nbsp;
												<Link to="/">Login</Link>
											</p>
											<div className="input-group">
												<input
													type="email"
													name="email"
													className="form-control"
													placeholder="Your Email Address"
													required
													value={this.state.email}
													onChange={this.handleChange.bind(this)}
												/>
												<span className="md-line" />
											</div>
											<div className="row">
												<div className="col-md-12">
													{this.state.msgsucc ? <SuccessfulMessage textmessage={this.state.msgtxt} /> : null}
													{this.state.msgerror ? <ErrorMessage textmessage={this.state.msgtxt} /> : null}
													<button
														type="button"
														className="btn btn-primary btn-md btn-block waves-effect text-center m-b-20"
														onClick={this.handleReset.bind(this)}>
														Reset and send me a new Password
													</button>
												</div>
											</div>
										</div>
									</form>
									{/* end of form */}
								</div>
								{/* Authentication card end */}
							</div>
							{/* end of col-sm-12 */}
						</div>
						{/* end of row */}
					</div>
					{/* end of container-fluid */}
				</section>
			</div>
		)
	}
}
export default Recov
