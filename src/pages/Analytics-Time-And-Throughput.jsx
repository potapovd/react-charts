import React from 'react'
import * as SETTINGS from '../settings.jsx'
import {userIsLogged, getHeadersXAuthToken} from '../actions/CheckUserAuth.jsx'
import {apiRequest} from '../actions/APIRequests.jsx'
import Menu from '../components/Menu.jsx'
import moment from 'moment'

//moth selector
import {MonthSelecter, getPeriod} from '../components/MonthSelecter.jsx'

class IndustrieFilterBu extends React.Component {
	handleChange(e) {
		console.log('some action 1 ', e.target.value)
	}
	render() {
		return (
			<div className="col-sm-12">
				<select name="select" className="form-control" onChange={this.handleChange.bind(this)}>
					<option value="opt1">BU</option>
					<option value="item1">item 2</option>
					<option value="item2">item 3</option>
					<option value="item3">item 4</option>
				</select>
			</div>
		)
	}
}
class IndustrieFilterCormTitle extends React.Component {
	handleChange(e) {
		console.log('some action 2 ', e.target.value)
	}
	render() {
		return (
			<div className="col-sm-12">
				<select name="select" className="form-control" onChange={this.handleChange.bind(this)}>
					<option value="opt1">Corporate title</option>
					<option value="item2">item 2</option>
					<option value="item3">item 3</option>
					<option value="item4">item 4</option>
				</select>
			</div>
		)
	}
}

class IndustrieStat extends React.Component {
	render() {
		return (
			<div className="col-xs-3 col-sm-3 col-lg-3">
				<span className="titleblock">{this.props.datum.title}</span>
				<h4>{Math.round(this.props.datum.value * 100) / 100}</h4>
			</div>
		)
	}
}

class CountsInComp extends React.Component {
	render() {
		const datum = this.props.datum
		//console.log(datum.length);
		let arrData = []
		for (let key in datum) {
			let vaue = datum[key]
			if (key === 'totalPositionsOpen') key = 'Positions Open'
			if (key === 'avgTurnAroundTime') key = 'Turn Around Time'
			if (key === 'avgTimeToOfferCandidate') key = 'Time To Offer Candidate'
			if (key === 'avgTimeToJoin') key = 'Time To Join'
			arrData.push({title: key, value: vaue})
		}

		return (
			<div className="col-md-12 col-lg-5 widget-profile-card-custom">
				<div className="card">
					<div className="card-block text-center">
						<div className="row">
							<div className="col-md-12 col-lg-6">
								<div className="form-group row">
									<IndustrieFilterBu />
								</div>
							</div>
							<div className="col-md-12 col-lg-6">
								<div className="form-group row">
									<IndustrieFilterCormTitle />
								</div>
							</div>
						</div>
					</div>
					<div className="card-footer bg-inverse">
						<div className="row text-center">
							{arrData.map((oneItem, index) => {
								return <IndustrieStat datum={oneItem} key={index} />
							})}
						</div>
					</div>
				</div>
			</div>
		)
	}
}

class CandidatOneState extends React.Component {
	render() {
		return (
			<div className="col-xs-2 col-sm-2 col-lg-2">
				<span>
					<span className="titleblock">{this.props.data.stage}</span>
					<h4>{this.props.data.noOfCandidates}</h4>
				</span>
			</div>
		)
	}
}

class CandidatState extends React.Component {

	render() {
		let datum = this.props.datum
		let sortedDatum = []
		for (let i = 0; i < datum.length; i++) {
			if (datum[i].stage === 'Screening') sortedDatum[0] = datum[i]
			if (datum[i].stage === 'Interviewing') sortedDatum[1] = datum[i]
			if (datum[i].stage === 'Test') sortedDatum[2] = datum[i]
			if (datum[i].stage === 'Select To Offer') sortedDatum[3] = datum[i]
			if (datum[i].stage === 'Offer') sortedDatum[4] = datum[i]
			if (datum[i].stage === 'Joined') sortedDatum[5] = datum[i]
		}
		return (
			<div className="col-md-12 col-lg-7 widget-profile-card-custom">
				<div className="card">
					<div className="card-block text-center">
						<h3 style={{marginBottom: '28px'}}>Jenny Joe</h3>
						{/*<p>Web Designer</p>*/}
						{/*<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci a, rem accusantium recusandae beatae.
						</p>*/}
					</div>
					<div className="card-footer bg-inverse">
						<div className="row text-center">
							{sortedDatum.map((line, index) => {
								return <CandidatOneState data={line} key={index} />
							})}
						</div>
					</div>
				</div>
			</div>
		)
	}
}

class AnalyticsTimeThroughput extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			candidDashb: [],
			jobmetrics: [],
			period: 'Oct-Dec'
		}
		this.apiReqest = this.apiReqest.bind(this)
		this.handelChangePeriod = this.handelChangePeriod.bind(this)
	}

	apiReqest() {
		let userToken = userIsLogged()
		const reqHeaders = getHeadersXAuthToken(userToken)
		let actaulPeriod = getPeriod(moment(), this.state.period)
		const apiCandidDashb = `${SETTINGS.API}dashboards/common/${actaulPeriod[0]}/${
			actaulPeriod[1]
		}/candidates/pipeline/`
		const apiJobmetrics = `${SETTINGS.API}dashboards/common/${actaulPeriod[0]}/${
			actaulPeriod[1]
		}/jobmetrics`
		this.setState({period: actaulPeriod[3]})
		apiRequest(apiCandidDashb, reqHeaders, 'post', 'analytics-time-throughput-candid-dashb').then(
			function(response) {
				this.setState({
					candidDashb: response.dashboardCandidates
				})
			}.bind(this)
		)
		apiRequest(apiJobmetrics, reqHeaders, 'post', 'analytics-time-throughput-jobmetrics').then(
			function(response) {
				this.setState({
					jobmetrics: response
				})
			}.bind(this)
		)
	}

	handelChangePeriod(newPeriod) {
		this.setState({period: newPeriod}, () => {
			this.apiReqest()
		})
	}

	componentDidMount() {
		this.apiReqest()
	}

	render() {
		return (
			<div>
				<Menu />

				<div className="main-body">
					<div className="page-wrapper">
						<div className="page-header">
							<div className="page-header-title">
								<h4>Time & Throughput Metrics</h4>
							</div>
						</div>
						<div className="page-body">
							<div className="row">
								<MonthSelecter
									changePeriond={this.handelChangePeriod.bind(this)}
									defaulPeriod={this.state.period}
								/>
							</div>
							<div className="row">
								<div className="col-md-12 col-lg-12">
									<div className="row">
										{<CountsInComp datum={this.state.jobmetrics} />}
										<CandidatState datum={this.state.candidDashb} />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default AnalyticsTimeThroughput
