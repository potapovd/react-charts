import React from 'react'
import LogoImg from '../assets/images/logo-hexagon-whitebg.png'

class page404 extends React.Component {
	render() {
		return (
			<div>
				<section className="login p-fixed d-flex text-center bg-primary common-img-bg">
					<div className="container-fluid">
						<div className="row">
							<div className="col-sm-12">
								<div className="login-card card-block auth-body">
									<form className="md-float-material">
										<div className="text-center">
											<img src={LogoImg} alt="logo-hexagon" />
											<h1>&nbsp;</h1>
											<h5>
												ERROR 404! <br /> Page not found.
											</h5>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		)
	}
}

export default page404
