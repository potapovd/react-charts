import React from 'react'
import * as SETTINGS from '../settings.jsx'
import {userIsLogged, getHeadersXAuthToken} from '../actions/CheckUserAuth.jsx'
import {apiRequest} from '../actions/APIRequests.jsx'
import Menu from '../components/Menu.jsx'
import moment from 'moment'

//charts and moth selector
import {MonthSelecter, getPeriod} from '../components/MonthSelecter.jsx'
import DiscreteBarChartDiv from '../components/charts/DiscreteBarChart.jsx'

class AnalyticsShowAnalysis extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			showRateCorporateTitle: [],
			showRatetimeRange: [],
			period: 'Oct-Dec'
		}
		this.apiReqest = this.apiReqest.bind(this)
	}

	apiReqest() {
		let userToken = userIsLogged()
		const reqHeaders = getHeadersXAuthToken(userToken)
		let actaulPeriod = getPeriod(moment(), this.state.period)
		const apiShowRateCorporateTitle = `${SETTINGS.API}dashboards/clients/showRate/${
			actaulPeriod[0]
		}/${actaulPeriod[1]}/corporateTitle`
		const apiShowRatetimeRange = `${SETTINGS.API}dashboards/clients/showRate/${actaulPeriod[0]}/${
			actaulPeriod[1]
		}/timeRange`
		this.setState({period: actaulPeriod[3]})
		apiRequest(apiShowRateCorporateTitle, reqHeaders, 'get', 'analytics').then(
			function(response) {
				this.setState({
					showRateCorporateTitle: response.showRates
				})
			}.bind(this)
		)
		apiRequest(apiShowRatetimeRange, reqHeaders, 'get', 'analytics').then(
			function(response) {
				this.setState({
					showRatetimeRange: response.showRates
				})
			}.bind(this)
		)
	}

	handelChangePeriod(newPeriod) {
		this.setState({period: newPeriod}, () => {
			this.apiReqest()
		})
	}

	componentDidMount() {
		this.apiReqest()
	}

	render() {
		return (
			<div>
				<Menu />
				<div className="main-body">
					<div className="page-wrapper">
						<div className="page-header">
							<div className="page-header-title">
								<h4>Analytics - IV Show Analysis</h4>
							</div>
						</div>
						<div className="page-body">
							<div className="row">
								<MonthSelecter
									changePeriond={this.handelChangePeriod.bind(this)}
									defaulPeriod={this.state.period}
								/>
							</div>
							<div className="row">
								<DiscreteBarChartDiv
									blockSize={'col-md-12 col-lg-6'}
									chartName={'Show/No Show % by Corporate Title'}
									chartData={this.state.showRateCorporateTitle}
									chartX={'x'}
									chartY={'y'}
								/>
								<DiscreteBarChartDiv
									blockSize={'col-md-12 col-lg-6'}
									chartName={'Show rates by IV time'}
									chartData={this.state.showRatetimeRange}
									chartX={'x'}
									chartY={'y'}
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default AnalyticsShowAnalysis
