import React from 'react'
import * as SETTINGS from '../settings.jsx'
import {userIsLogged, getHeadersXAuthToken} from '../actions/CheckUserAuth.jsx'
import {apiRequest} from '../actions/APIRequests.jsx'
import Menu from '../components/Menu.jsx'

import PieChartDiv from '../components/charts/PieChart.jsx'

class JobsDashboard extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			jobsbu: [],
			jobslevel: [],
			jobsfulfillment: [],
			jobsageing: [],
			jobsstatus: [],
			jobsfultag: []
		}
	}
	componentDidMount() {
		let userToken = userIsLogged()
		const reqHeaders = getHeadersXAuthToken(userToken)

		const apiJobsBu = `${SETTINGS.API}dashboards/common/01-10-2017/01-01-2018/jobs/bu`
		const apiJobsLevel = `${SETTINGS.API}dashboards/common/09-07-2017/09-01-2018/jobs/level`
		const apiJobsFulfillment = `${
			SETTINGS.API
		}dashboards/common/09-07-2017/09-01-2018/jobs/fulfillment/ageing`
		const apiJobsAgeing = `${SETTINGS.API}dashboards/common/09-07-2017/09-01-2018/jobs/ageing`
		const apiJobsStatus = `${SETTINGS.API}dashboards/common/09-07-2017/09-01-2018/jobs/status`
		const apiJobsTag = `${SETTINGS.API}dashboards/common/09-07-2017/09-01-2018/jobs/tag`

		apiRequest(apiJobsBu, reqHeaders, 'post', 'jobs').then(
			function(response) {
				this.setState({jobsbu: response.dashboardJobs})
			}.bind(this)
		)
		apiRequest(apiJobsLevel, reqHeaders, 'post', 'jobs').then(
			function(response) {
				this.setState({jobslevel: response.dashboardJobs})
			}.bind(this)
		)
		apiRequest(apiJobsFulfillment, reqHeaders, 'post', 'jobs').then(
			function(response) {
				this.setState({jobsfulfillment: response.dashboardJobs})
			}.bind(this)
		)
		apiRequest(apiJobsAgeing, reqHeaders, 'post', 'jobs').then(
			function(response) {
				this.setState({jobsageing: response.dashboardJobs})
			}.bind(this)
		)
		apiRequest(apiJobsStatus, reqHeaders, 'post', 'jobs').then(
			function(response) {
				this.setState({jobsstatus: response.dashboardJobs})
			}.bind(this)
		)
		apiRequest(apiJobsTag, reqHeaders, 'post', 'jobs').then(
			function(response) {
				this.setState({jobsfultag: response.dashboardJobs})
			}.bind(this)
		)
	}

	render() {
		return (
			<div>
				<Menu />
				<div className="main-body">
					<div className="page-wrapper">
						<div className="page-header">
							<div className="page-header-title">
								<h4>Jobs - Dashboard</h4>
							</div>
						</div>
						<div className="page-body">
							<div className="row">
								<PieChartDiv
									blockSize={'col-md-12 col-lg-4'}
									chartName={'Jobs By BU'}
									chartData={this.state.jobsbu}
									chartX={'bu'}
									chartY={'noOfJobs'}
								/>
								<PieChartDiv
									blockSize={'col-md-12 col-lg-4'}
									chartName={'Jobs By level'}
									chartData={this.state.jobslevel}
									chartX={'level'}
									chartY={'noOfJobs'}
								/>
								<PieChartDiv
									blockSize={'col-md-12 col-lg-4'}
									chartName={'Jobs By Fulfillment'}
									chartData={this.state.jobsfulfillment}
									chartX={'candidateGroup'}
									chartY={'noOfJobs'}
								/>
							</div>
							<div className="row">
								<PieChartDiv
									blockSize={'col-md-12 col-lg-4'}
									chartName={'Jobs By Ageing'}
									chartData={this.state.jobsageing}
									chartX={'ageingGroup'}
									chartY={'noOfJobs'}
								/>
								<PieChartDiv
									blockSize={'col-md-12 col-lg-4'}
									chartName={'Jobs By Status'}
									chartData={this.state.jobsstatus}
									chartX={'status'}
									chartY={'noOfJobs'}
								/>
								<PieChartDiv
									blockSize={'col-md-12 col-lg-4'}
									chartName={'Jobs By Function'}
									chartData={this.state.jobsfulfillment}
									chartX={'tag'}
									chartY={'noOfJobs'}
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default JobsDashboard
