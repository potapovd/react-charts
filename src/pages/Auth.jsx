import React from 'react'
import cookie from 'react-cookies'
import {Link} from 'react-router-dom'
import * as SETTINGS from '../settings.jsx'
import {apiRequest} from '../actions/APIRequests.jsx'

import LogoImg from '../assets/images/logo-hexagon-whitebg.png'

class ErrorMessage extends React.Component {
	render() {
		return (
			<div className="alert alert-danger background-danger hiddenalerts">
				<strong>{this.props.textmessage}</strong>
			</div>
		)
	}
}

class Auth extends React.Component {
	constructor(props) {
		super(props)
		this.state = {email: '', password: '', msgtxt: '', msgerror: false}
		this.handleChange = this.handleChange.bind(this)
		this.handleAuth = this.handleAuth.bind(this)
	}

	handleChange({target}) {
		this.setState({[target.name]: target.value})
	}

	handleAuth() {
		if (this.state.password.length > 2 && this.state.email.length > 2) {
			this.setState({msgerror: false, msgsucc: false})

			const authHeader = new Headers({
				'Content-Type': 'application/json'
			})
			let authFlag = {email: this.state.email, password: this.state.password}
			const apiURL = `${SETTINGS.APISCHORT}auth/login`
			apiRequest(apiURL, authHeader, 'post', 'auth', authFlag).then(function(response) {
				//Auth - Successful
				console.log('RequestAuth - Successful')
				cookie.save('userId', response.userId, {path: '/'})
				cookie.save('userName', response.userName, {path: '/'})
				cookie.save('companyId', response.companyId, {path: '/'})
				cookie.save('userToken', response.token, {path: '/'})
				//document.location = "/jobs-dashboard";
				document.location = '#/jobs-dashboard'
				return true
			})
		}
	}
	componentDidMount() {
		if (cookie.load('userToken')) {
			document.location = '#/jobs-dashboard'
		}
	}

	render() {
		return (
			<div>
				<section className="login p-fixed d-flex text-center bg-primary common-img-bg">
					{/* Container-fluid starts */}
					<div className="container-fluid">
						<div className="row">
							<div className="col-sm-12">
								{/* Authentication card start */}
								<div className="login-card card-block auth-body">
									<form className="md-float-material">
										<div className="text-center">
											<img src={LogoImg} alt="logo-hexagon" />
											<h1>HEXAGON</h1>
										</div>
										<div className="auth-box">
											<div className="row m-b-20">
												<div className="col-md-12">
													<h3 className="text-left txt-primary">Sign In</h3>
												</div>
											</div>
											<hr />
											<div className="input-group">
												<input
													type="email"
													name="email"
													className="form-control"
													placeholder="Your Email Address"
													required
													value={this.state.email}
													onChange={this.handleChange.bind(this)}
												/>
												<span className="md-line" />
											</div>
											<div className="input-group">
												<input
													type="password"
													name="password"
													className="form-control"
													placeholder="Password"
													required
													value={this.state.password}
													onChange={this.handleChange.bind(this)}
												/>
												<span className="md-line" />
											</div>
											<div className="row m-t-25 text-left">
												<div className="col-sm-7 col-xs-12">
													<div className="checkbox-fade fade-in-primary">
														<label>
															<input type="checkbox" defaultValue />
															<span className="cr">
																<i className="cr-icon icofont icofont-ui-check txt-primary" />
															</span>
															<span className="text-inverse">Remember me</span>
														</label>
													</div>
												</div>
												<div className="col-sm-5 col-xs-12 forgot-phone text-right">
													<Link to="/recovery" className="text-right f-w-600 text-inverse">
														Forgot Your Password?
													</Link>
												</div>
											</div>
											<div className="row m-t-30">
												<div className="col-md-12">
													{this.state.msgerror ? <ErrorMessage textmessage={this.state.msgtxt} /> : null}
													<button
														type="button"
														className="btn btn-primary btn-md btn-block waves-effect text-center m-b-20"
														onClick={this.handleAuth.bind(this)}>
														Sign in
													</button>
												</div>
											</div>
										</div>
									</form>
									{/* end of form */}
								</div>
								{/* Authentication card end */}
							</div>
							{/* end of col-sm-12 */}
						</div>
						{/* end of row */}
					</div>
					{/* end of container-fluid */}
				</section>
			</div>
		)
	}
}
export default Auth
