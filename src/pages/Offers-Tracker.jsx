import React from 'react'
import cookie from 'react-cookies'
import Moment from 'react-moment'
import * as SETTINGS from '../settings.jsx'
import {userIsLogged, getHeadersXAuthToken} from '../actions/CheckUserAuth.jsx'
import {apiRequest} from '../actions/APIRequests.jsx'
import Menu from '../components/Menu.jsx'

//modals info
import OneUserModal from '../components/modals/offers/OneUserModal.jsx'
import horizontalTimelineRunner from '../assets/js/horizontaltimeline.js'

//table and popdals scripts
import '../assets/css/dataTables.bootstrap4.min.css'
import $ from 'jquery'
let jQuery = window.jQuery  = window.$ = $
$.DataTable = require('datatables.net')
require('datatables.net-bs4')
require('popper.js')
require('bootstrap')

class OnelineTrackerJob extends React.Component {
	getProbabilityHTML() {
		if (this.props.tabdata.offerDetail.todoPercentage <= 0) return 'WIP'
		if (this.props.tabdata.showProbability === 'H')
			return <label className="label label-md label-success">On Track</label>
		if (this.props.tabdata.showProbability === 'W')
			return <label className="label label-md label-warning">Erratic</label>
		if (this.props.tabdata.showProbability === 'C')
			return <label className="label label-md label-danger">On Thin Ice</label>
	}

	getProgressHTML() {
		return (
			<div className="progress" style={{width: 100}}>
				<div
					className="progress-bar progress-bar-striped progress-lg progress-bar-info"
					role="progressbar"
					style={{
						width: this.props.tabdata.offerDetail.todoPercentage,
						height: 23,
						backgroundColor: '#12afcb87'
					}}
					aria-valuemin="0"
					aria-valuemax="100">
					<span
						style={{color: 'darkslategrey', float: 'left', marginLeft: 8, marginTop: 7, fontSize: 11}}>
						{this.props.tabdata.offerDetail.todoPercentage + '%'}
					</span>
				</div>
			</div>
		)
	}

	render() {

		let joiningDate
		if (Number.isInteger(this.props.tabdata.joiningDate)) {
			joiningDate = this.props.tabdata.joiningDate / 1000
		}
		let offeredDate
		if (Number.isInteger(this.props.tabdata.offeredDate)) {
			offeredDate = this.props.tabdata.offeredDate / 1000
		}
		let doj
		if (Number.isInteger(joiningDate)) {
			let nowtime = new Date().getTime()
			doj = Math.round((joiningDate * 1000 - nowtime) / 1000 / 60 / 60 / 24)
		}

		let className = this.props.activeLine === this.props.lineId ? 'active' : ''
		return (
			<tr
				className={className}
				onClick={this.props.handelChangeActiveLine.bind(this, this.props.tabdata.id)}>
				<td>
					<a
						data-toggle="modal"
						href="#large-Modal-OneUser"
						className="modalaction"
						onClick={this.props.handelGetCandId.bind(this, {
							candId: this.props.tabdata.id,
							jobId: this.props.tabdata.job.id
						})}>
						{this.props.tabdata.candidate.firstName}&nbsp;
						{this.props.tabdata.candidate.lastName}
					</a>
				</td>
				<td>{this.props.tabdata.job.designation}</td>
				<td>{this.props.tabdata.stage}</td>
				<td>{this.getProbabilityHTML()}</td>
				<td>{this.getProgressHTML()}</td>
				<td>{this.props.tabdata.job.corporateTitle}</td>
				<td>{this.props.tabdata.job.employerCompany.shortname}</td>
				<td>
					{joiningDate ? (
						<Moment unix format="DD-MM-YYYY">
							{joiningDate}
						</Moment>
					) : (
						''
					)}
				</td>
				<td>
					{offeredDate ? (
						<Moment unix format="DD-MM-YYYY">
							{joiningDate}
						</Moment>
					) : (
						''
					)}
				</td>
				<td>{doj}</td>
				<td>{this.props.tabdata.clientSPOC}</td>
			</tr>
		)
	}
}

class OffersTracker extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			tabdata: [],
			activeLine: '',
			candId: '',
			jobId: '',
			timelineData: []
		}
		this.handelChangeActiveLine = this.handelChangeActiveLine.bind(this)
		this.handelGetCandId = this.handelGetCandId.bind(this)
	}

	handelChangeActiveLine(event) {
		this.setState({activeLine: event})
	}

	componentDidMount() {
		let userToken = userIsLogged()
		const reqHeaders = getHeadersXAuthToken(userToken)
		const apiURL = `${SETTINGS.API}cnjobprocess/STOOfferedJoined`
		const flag = cookie.load('companyId')
		apiRequest(apiURL, reqHeaders, 'post', 'offers-tracker', flag).then(
			function(response) {
				this.setState(
					{
						tabdata: response
					},
					() => {
						$(this.tableSmart).DataTable()
					}
				)
			}.bind(this)
		)
	}

	handelGetCandId(e) {
		console.log(e)
		this.setState(
			{
				candId: e.candId,
				jobId: e.jobId
			},
			() => {
				let userToken = userIsLogged()
				const reqHeaders = getHeadersXAuthToken(userToken)
				// DO NOT REMOVE ME!!!!!!!
				const apiURL = `${SETTINGS.API}/todos/candidates/${this.state.candId}?jobId=${
					this.state.jobId
				}&stage=Joined`
				//const apiURL = `${SETTINGS.API}/todos/candidates/28452?jobId=3692&stage=Joined`;
				apiRequest(apiURL, reqHeaders, 'get', 'offers-tracker').then(
					function(response) {
						this.setState(
							{
								timelineData: response
							},
							() => {
								horizontalTimelineRunner()
							}
						)
					}.bind(this)
				)
			}
		)
	}

	render() {
		return (
			<div>
				<Menu />
				<div className="main-body">
					<div className="page-wrapper">
						<div className="page-header">
							<div className="page-header-title">
								<h4>Offers - Tracker</h4>
							</div>
						</div>
						<div className="page-body">
							<div className="row">
								{/* User detail table start */}
								<div className="col-xl-12">
									<div className="row">
										<div className="col-sm-12">
											<div className="card table-1-card">
												<div className="card-block">
													<div className="table-responsive">
														<table
															className="table"
															id="tableSmart"
															ref={(tableSmart) => (this.tableSmart = tableSmart)}>
															<thead>
																<tr className="text-capitalize">
																	<th>Candidate Name</th>
																	<th>Job Title</th>
																	<th>Stage</th>
																	<th>Status</th>
																	<th>Progress</th>
																	<th>Corporate Title</th>
																	<th>BU</th>
																	<th>Offer Date</th>
																	<th>Joining Date</th>
																	<th>DOJ CD</th>
																	<th>HM/SPOC</th>
																</tr>
															</thead>
															<tbody>
																{this.state.tabdata.map((tabOneLine) => (
																	<OnelineTrackerJob
																		key={tabOneLine.id}
																		tabdata={tabOneLine}
																		activeLine={this.state.activeLine}
																		lineId={tabOneLine.id}
																		handelChangeActiveLine={this.handelChangeActiveLine.bind(this)}
																		handelGetCandId={this.handelGetCandId.bind(this)}
																	/>
																))}
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								{/* User detail table end */}
								<OneUserModal timelineData={this.state.timelineData} />
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
export default OffersTracker
