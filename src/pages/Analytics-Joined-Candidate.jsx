import React from 'react'
import * as SETTINGS from '../settings.jsx'
import {userIsLogged, getHeadersXAuthToken} from '../actions/CheckUserAuth.jsx'
import {apiRequest} from '../actions/APIRequests.jsx'
import Menu from '../components/Menu.jsx'
import moment from 'moment'

//charts and moth selector
import {MonthSelecter, getPeriod} from '../components/MonthSelecter.jsx'
import DiscreteBarChart from '../components/charts/DiscreteBarChart.jsx'
import PieChartDiv from '../components/charts/PieChart.jsx'

//testdata
var tmpCandTopFrom = [
	{
		key: 'Group 0',
		values: [
			{
				x: 0.7943146464619459,
				y: -2.0373950071226035,
				size: 0.44371137031535346
			},
			{
				x: -0.1522479942337807,
				y: -0.2504109527984302,
				size: 0.03674736533595668
			},
			{
				x: 0.44110985335522596,
				y: -0.16311200057636885,
				size: 0.793519827522364
			}
		]
	},
	{
		key: 'Group 1',
		values: [
			{
				x: 0.6551104484819862,
				y: -0.02662105931777487,
				size: 0.3422465475997847
			},
			{
				x: 0.7415532332387774,
				y: 0.7170164185338105,
				size: 0.6737963330536276
			},
			{
				x: -0.9134179841748119,
				y: -0.15890861849008003,
				size: 0.43891371068498586
			}
		]
	},
	{
		key: 'Group 2',
		values: [
			{
				x: -1.1699161115843097,
				y: -0.8493123842575784,
				size: 0.6020933031666253
			},
			{
				x: -0.939441437882209,
				y: 1.8312346158251425,
				size: 0.9637063348540178
			},
			{
				x: 0.6743953393174396,
				y: 0.8293394731330828,
				size: 0.45335285909851475
			}
		]
	},
	{
		key: 'Group 3',
		values: [
			{
				x: -0.33186647209655495,
				y: 1.9450334207001219,
				size: 0.3869392921717354
			},
			{
				x: 1.3018659522300648,
				y: 0.5443233771825304,
				size: 0.09932886172622046
			},
			{
				x: -1.0572046724080306,
				y: -1.246211299549501,
				size: 0.25905056930188963
			}
		]
	}
]

class AnalyticsJoinedCandidate extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			candByGen: [],
			candByEdu: [],
			candByLoc: [],
			candBySour: [],
			candTopFrom: [],
			candExpTitle: [],
			period: 'Oct-Dec'
		}
		this.apiReqest = this.apiReqest.bind(this)
	}

	apiReqest() {
		let userToken = userIsLogged()
		const reqHeaders = getHeadersXAuthToken(userToken)
		let actaulPeriod = getPeriod(moment(), this.state.period)
		const apiCandByGen = `${SETTINGS.API}dashboards/clients/joined/${actaulPeriod[0]}/${
			actaulPeriod[1]
		}/gender`
		const apiCandByEdu = `${SETTINGS.API}dashboards/clients/joined/${actaulPeriod[0]}/${
			actaulPeriod[1]
		}/education`
		const apiCandByLoc = `${SETTINGS.API}dashboards/clients/joined/${actaulPeriod[0]}/${
			actaulPeriod[1]
		}/location`
		const apiCandBySour = `${SETTINGS.API}dashboards/clients/joined/${actaulPeriod[0]}/${
			actaulPeriod[1]
		}/source`
		const apiCandTopFrom = `${SETTINGS.API}dashboards/clients/top10Companies/${actaulPeriod[0]}/${
			actaulPeriod[1]
		}`
		//const apiCandExpTitle = `${SETTINGS.API}dashboards/clients/experience/${actaulPeriod[0]}/${actaulPeriod[1]}`;
		this.setState({period: actaulPeriod[3]})
		apiRequest(apiCandByGen, reqHeaders, 'get', 'analytics').then(
			function(response) {
				this.setState({candByGen: response.joinedStats})
			}.bind(this)
		)
		apiRequest(apiCandByEdu, reqHeaders, 'get', 'analytics').then(
			function(response) {
				this.setState({candByEdu: response.joinedStats})
			}.bind(this)
		)
		apiRequest(apiCandByLoc, reqHeaders, 'get', 'analytics').then(
			function(response) {
				this.setState({candByLoc: response.joinedStats})
			}.bind(this)
		)
		apiRequest(apiCandBySour, reqHeaders, 'get', 'analytics').then(
			function(response) {
				this.setState({candBySour: response.joinedStats})
			}.bind(this)
		)
		apiRequest(apiCandTopFrom, reqHeaders, 'get', 'analytics').then(
			function(response) {
				this.setState({candTopFrom: response.topCompaniesHiredFrom})
			}.bind(this)
		)
	}

	handelChangePeriod(newPeriod) {
		this.setState({period: newPeriod}, () => {
			this.apiReqest()
		})
	}

	componentDidMount() {
		this.apiReqest()
	}

	render() {
		return (
			<div>
				<Menu />
				<div className="main-body">
					<div className="page-wrapper">
						<div className="page-header">
							<div className="page-header-title">
								<h4>Analytics - Joined Candidate Analysis</h4>
							</div>
						</div>
						<div className="page-body">
							<div className="row">
								<MonthSelecter
									changePeriond={this.handelChangePeriod.bind(this)}
									defaulPeriod={this.state.period}
								/>
							</div>
							<div className="row">
								<PieChartDiv
									blockSize={'col-md-12 col-lg-4'}
									chartName={'Candidates By Gender'}
									chartData={this.state.candByGen}
									chartX={'gender'}
									chartY={'ct'}
								/>
								<PieChartDiv
									blockSize={'col-md-12 col-lg-4'}
									chartName={'Candidates By Edu'}
									chartData={this.state.candByEdu}
									chartX={'education'}
									chartY={'ct'}
								/>
								<PieChartDiv
									blockSize={'col-md-12 col-lg-4'}
									chartName={'Candidates By Location'}
									chartData={this.state.candByLoc}
									chartX={'location'}
									chartY={'ct'}
								/>
							</div>
							<div className="row">
								<PieChartDiv
									blockSize={'col-md-12 col-lg-4'}
									chartName={'Candidates By Source'}
									chartData={this.state.candBySour}
									chartX={'location'}
									chartY={'ct'}
								/>
								<DiscreteBarChart
									blockSize={'col-md-12 col-lg-8'}
									chartName={'CTop 10 companies hired from'}
									chartData={this.state.candTopFrom}
									chartX={'x'}
									chartY={'t'}
								/>

								{/*<div className="col-md-12 col-lg-4">
									<div className="card">
										<div className="card-header">
											<h5>Experience by Corporate Title</h5>
											<span></span>
										</div>
										<div className="card-block">
											<DiscreteBarChart
													datum={this.state.candExpTitle}
													x="x"
													y="t"
												/>
										</div>
									</div>
								</div>*/}
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
export default AnalyticsJoinedCandidate
