import React from 'react'
import * as SETTINGS from '../settings.jsx'
import {userIsLogged, getHeadersXAuthToken} from '../actions/CheckUserAuth.jsx'
import {apiRequest} from '../actions/APIRequests.jsx'
import Menu from '../components/Menu.jsx'

import PieChartDiv from '../components/charts/PieChart.jsx'
import MultiBarChartDiv from '../components/charts/MultiBarChart.jsx'

class OffersDashboard extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			offersbu: [],
			offweslevel: [],
			offersstatus: [],
			offersbymontstatus: []
		}
	}
	componentDidMount() {
		let userToken = userIsLogged()
		const reqHeaders = getHeadersXAuthToken(userToken)

		const apiOffersBu = `${SETTINGS.API}dashboards/clients/offers/bu`
		const apiOffersLevel = `${SETTINGS.API}dashboards/clients/offers/level`
		const apiOffersStatus = `${SETTINGS.API}dashboards/clients/offers/status`
		const apiOffersMonthStatus = `${SETTINGS.API}dashboards/clients/offers/monthStatus`

		apiRequest(apiOffersBu, reqHeaders, 'get', 'offers').then(
			function(response) {
				this.setState({offersbu: response.offersByBU})
			}.bind(this)
		)
		apiRequest(apiOffersLevel, reqHeaders, 'get', 'offers').then(
			function(response) {
				this.setState({offweslevel: response.offersByLevel})
			}.bind(this)
		)
		apiRequest(apiOffersStatus, reqHeaders, 'get', 'offers', 'offersstatus').then(
			function(response) {
				this.setState({offersstatus: response.offersByStatus})
			}.bind(this)
		)
		apiRequest(apiOffersMonthStatus, reqHeaders, 'get', 'offers', 'offersbymontstatus').then(
			function(response) {
				this.setState({offersbymontstatus: response.offersByMonthStatus})
			}.bind(this)
		)
	}

	render() {
		return (
			<div>
				<Menu />
				<div className="main-body">
					<div className="page-wrapper">
						<div className="page-header">
							<div className="page-header-title">
								<h4>Offers - Dashboard</h4>
							</div>
						</div>
						<div className="page-body">
							<div className="row">
								<PieChartDiv
									blockSize={'col-md-12 col-lg-4'}
									chartName={'Offers By BU'}
									chartData={this.state.offersbu}
									chartX={'buName'}
									chartY={'noOfJobs'}
								/>
								<PieChartDiv
									blockSize={'col-md-12 col-lg-4'}
									chartName={'Offers By level'}
									chartData={this.state.offweslevel}
									chartX={'jobLevel'}
									chartY={'noOfJobs'}
								/>
								<PieChartDiv
									blockSize={'col-md-12 col-lg-4'}
									chartName={'Offers By Status'}
									chartData={this.state.offersstatus}
									chartX={'showProbability'}
									chartY={'offers'}
								/>
							</div>

							<div className="row">
								<MultiBarChartDiv
									blockSize={'col-md-12 col-lg-12'}
									chartName={'Future Joinees By Month and Status'}
									chartData={this.state.offersbymontstatus}
									chartX={'label'}
									chartY={'values'}
									chartColor={[SETTINGS.COLOR_RED, SETTINGS.COLOR_YELLOW, SETTINGS.COLOR_GREEN]}
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default OffersDashboard
