import React from 'react'
import * as SETTINGS from '../settings.jsx'
import {userIsLogged, getHeadersXAuthToken} from '../actions/CheckUserAuth.jsx'
import {apiRequest} from '../actions/APIRequests.jsx'
import Menu from '../components/Menu.jsx'
import moment from 'moment'

//charts and moth selector
import {MonthSelecter, getPeriod} from '../components/MonthSelecter.jsx'
import PieChartDiv from '../components/charts/PieChart.jsx'

class AnalyticsReasonsForRejection extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			stageRejectReason: [],
			stageRejectReasonSTO: [],
			stageRejectReasonOffer: [],
			period: 'Oct-Dec'
		}
		this.apiReqest = this.apiReqest.bind(this)
	}

	apiReqest() {
		let userToken = userIsLogged()
		const reqHeaders = getHeadersXAuthToken(userToken)
		let actaulPeriod = getPeriod(moment(), this.state.period)
		//DO NOT REMOVE ME!!!
		// const apiStageRejectReason = `${SETTINGS.API}dashboards/clients/stageRejectReason/${
		// 	actaulPeriod[0]
		// }/${actaulPeriod[1]}`
		const apiStageRejectReasonSTO = `${SETTINGS.API}dashboards/clients/stageRejectReason/${
			actaulPeriod[0]
		}/${actaulPeriod[1]}/Select To Offer`
		const apiStageRejectReasonOffer = `${SETTINGS.API}dashboards/clients/stageRejectReason/${
			actaulPeriod[0]
		}/${actaulPeriod[1]}/Offer`

		this.setState({period: actaulPeriod[3]})
		//DO NOT REMOVE ME!!!
		// apiRequest(apiStageRejectReason, reqHeaders, "get", "analytics").then(function (response) {
		//     this.setState({
		//         stageRejectReason: response.rejectReasons
		//     });
		// }.bind(this));
		apiRequest(apiStageRejectReasonSTO, reqHeaders, 'get', 'analytics').then(
			function(response) {
				this.setState({
					stageRejectReasonSTO: response.rejectReasons
				})
			}.bind(this)
		)
		apiRequest(apiStageRejectReasonOffer, reqHeaders, 'get', 'analytics').then(
			function(response) {
				this.setState({
					stageRejectReasonOffer: response.rejectReasons
				})
			}.bind(this)
		)
	}

	handelChangePeriod(newPeriod) {
		this.setState({period: newPeriod}, () => {
			this.apiReqest()
		})
	}

	componentDidMount() {
		this.apiReqest()
	}

	render() {
		return (
			<div>
				<Menu />

				<div className="main-body">
					<div className="page-wrapper">
						<div className="page-header">
							<div className="page-header-title">
								<h4>Analytics - Reasons for Rejection</h4>
							</div>
						</div>
						<div className="page-body">
							<div className="row">
								<MonthSelecter
									changePeriond={this.handelChangePeriod.bind(this)}
									defaulPeriod={this.state.period}
								/>
							</div>
							<div className="row">
								<PieChartDiv
									blockSize={'col-md-12 col-lg-6'}
									chartName={'Offer Declines By STO'}
									chartData={this.state.stageRejectReasonSTO}
									chartX={'reasons'}
									chartY={'ct'}
								/>
								<PieChartDiv
									blockSize={'col-md-12 col-lg-6'}
									chartName={'Offer Declines By Offered'}
									chartData={this.state.stageRejectReasonOffer}
									chartX={'reasons'}
									chartY={'ct'}
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default AnalyticsReasonsForRejection
