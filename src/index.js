import React from 'react'
import {render} from 'react-dom'

//Routing
import {HashRouter, Switch, Route} from 'react-router-dom'
import Auth from './pages/Auth.jsx'
import Recov from './pages/Recov.jsx'
import JobsDashboard from './pages/Jobs-Dashboard.jsx'
import JobsTracker from './pages/Jobs-Tracker.jsx'
import OffersDashboard from './pages/Offers-Dashboard.jsx'
import OffersTracker from './pages/Offers-Tracker.jsx'
import AnalyticsTimeThroughput from './pages/Analytics-Time-And-Throughput.jsx'
import AnalyticsReasonsForRejection from './pages/Analytics-Reasons-For-Rejection.jsx'
import AnalyticsShowAnalysis from './pages/Analytics-Show-Analysis.jsx'
import AnalyticsJoinedCandidate from './pages/Analytics-Joined-Candidate.jsx'
import Page404 from './pages/Page404.jsx'
import './assets/css/style-custom.css'

render(
	<HashRouter>
		<Switch>
			<Route exact path="/" component={Auth} />
			<Route path="/recovery" component={Recov} />
			<Route path="/jobs-dashboard" component={JobsDashboard} />
			<Route path="/jobs-tracker" component={JobsTracker} />
			<Route path="/offers-dashboard" component={OffersDashboard} />
			<Route path="/offers-tracker" component={OffersTracker} />
			<Route path="/analytics-time-and-throughput" component={AnalyticsTimeThroughput} />
			<Route path="/analytics-reasons-for-rejection" component={AnalyticsReasonsForRejection} />
			<Route path="/analytics-show-analysis" component={AnalyticsShowAnalysis} />
			<Route path="/analytics-joined-candidate" component={AnalyticsJoinedCandidate} />
			<Route path="*" component={Page404} />
		</Switch>
	</HashRouter>,
	document.getElementById('app')
)
